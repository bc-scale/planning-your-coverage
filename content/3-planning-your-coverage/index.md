# 3. Planning Your Coverage

Individual reporters and editors plan much of their coverage based on a calendar for coverage. It is important that you establish good sources in the beats you cover so you know what is happening when. Conferences, sporting events, artistic performances are all scheduled. In most cases the organizers want media coverage and will promote the event well ahead of time. Some events are scheduled on very short notice, press conferences for example. Other events are not scheduled at all. Having a calendar, even if you are constantly adjusting it, will ensure that you are producing many of your stories on a planned schedule rather than merely reacting to events or coverage from other publications.

Most news organizations have editorial meetings once a day, typically in the morning. Some meet twice a day. Depending on the size of the publication, sometimes only editors attend the meeting. The meeting allows the news organization to plan coverage for the day as well as discuss calendar events scheduled a bit farther out. Reporters or editors give the group a summary of their story and a plan for coverage. The group in turn provides feedback intended to enhance coverage. These meetings are normally brief but are an essential part of a news organizations operation.

---

The core of good reporting is to be proactive, not reactive, in your coverage.

Here are some characteristics of proactive reporting:

---

## Talk to you audience

* online and off, what issues interest your audience? What information are contributors and sources giving you that you could turn into a story or fold into your coverage of an event?

---

## Talk to your editor

* you should be in regular contact with editors. This might be in the news meeting or informally on the phone or in the office. Editors can help you plan your coverage and coordinate it with other stories. They may also assign you stories to cover.

---

## Collect and organize information

* You need to keep and organize important material related to the stories you cover. This might be links to source documents, previous coverage of the issue, promising contacts, and other useful tips. Make bookmarks of links. Keep a list of helpful contacts. Archive previous coverage of the issue.

---

## Promote, plan ahead

* Depending on which social platforms you use and how your news organization’s site is structured, you need to promote your coverage, and build interest in it. Let your audience know what’s coming up. You also need to think through what kind of media you’ll need to best report your story.

---

Let’s look at planning for the three most basic types of coverage:

* **THE PLANNED EVENT**
* **THE TREND STORY**
* **BREAKING NEWS**

How might you prepare and report each throughout the news cycle for the story? The more systematic your reporting is, the better your coverage will be. Many stories will not fit perfectly into the three categories. Some will move from one category to the next over time. Many of the tips for coverage below can apply to more than one type of story.

---
