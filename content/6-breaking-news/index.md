# 6. Breaking News

### Breaking news stories are unscheduled stories that require immediate coverage. A natural disaster, an unplanned announcement by a public official, a bombing, or the death of an important public official. Breaking news requires reporters to respond immediately.

---

## Move quickly

When a breaking news event happens, drop everything and call your editor. You may need to coordinate coverage with others. Your earliest report may be little more than a basic dispatch: who, what, where, when, how and why. Go to your audience for information.

---

## Develop story

As information comes in you need to build out your story. Get photos and videos up as soon as possible. Provide background or context to your initial reporting. Go to any pre-reported material you have.

---

## Verify and be transparent

Go to sources who can verify first hand what you are hearing. Check with credible authorities and witnesses. Cross reference your information. And, always tell people where your information is coming from.

---

## Tie it to a broader issue

Tie the story to broader trends or news events as soon as you can. This will help you move through the news cycle from breaking news coverage toward more detailed and deeper coverage.

---
