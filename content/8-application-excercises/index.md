# 8. Application / Exercises

## Story Meeting

Identifying priority stories from random lists. Classifying example stories into types.

---

## Newsroom Simulation

Students take on coverage roles for an issue proximate to training locale. Assignments are given, coverage is assembled by editors.

---

## Case Histories

You are the managing editor. You have finite resources. Given the constraints, deploy your assets to cover a breaking story. Put realistic constraints in place so students have to make tough editorial decisions.

---

## Market Research

Write a report on what readers you have, what readers you’d like to have, and what each group wants to know about.

---
