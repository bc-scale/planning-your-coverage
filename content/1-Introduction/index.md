# 1. Introduction

Your coverage will inevitably change and expand as the story evolves. This progression is called the news cycle. Smart journalists anticipate the news cycle and stay one step ahead of it, providing new, richer information along the way. Your coverage may begin with a few exploratory Tweets or a brief post -- little more than a headline and photo -- on Facebook. As the story develops you may add video(s), engage the audience in dialogue, link to information about the event from other sources, even provide information such as maps and source documents. Overtime, it’s likely you’ll engage a much broader audience across an assortment of platforms. Again, good journalists anticipate this cycle, collecting and then distributing information as it is developed and verified.

This module will walk you through strategies individual reporters and editors may use to plan and execute on coverage of breaking and planned events, from before coverage begins, to closing out a story. A case study is provided, along with some samples exercises to get you thinking about a coverage plan like a reporter.

Like the other modules in the series, it assumes the use of StoryMaker for the production of content. It also assumes a social-first approach to news coverage. For more about social-first journalism and how a social-first news agency works, check out “Crafting a Social-First News Strategy,” a guide to developing a social-first editorial plan.

---
