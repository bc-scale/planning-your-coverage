# 7.1 Assigning Stories

The editors selected stories that worked to fill out the overall story chronology and that seemed accessible by the project’s deadline. In addition to reporters, the project also had the following staff assignments.

---

## Project Manager

The project manager, an editor, was placed in charge of the entire project. They reported to the editor-in-chief, helped solve problems and made sure things moved toward deadline.

---

## Web Editor

The web editor made sure that the final project could be presented and function properly online. This included implementing design decisions and making sure that content was formatted to flow properly.

---

## Social Media Editor

The social media editor helped to promote the story and build out the crowd sourced elements of the project.

---

### Print Editors

The print editor edited the print elements of each story.

---

### Multimedia editors

The multimedia editor handled the photo and video elements of the project, coordinating with the print editor.

---

### Outreach/Sources Coordinator

Because it was such a large project and required a great deal of organization around access to New York City courts and detention centers, a coordinator was assigned. The coordinator also helped reporters find and share really good sources.

---

The project manager laid out clear and realistic milestones for the team as the project progressed toward the final deadline. The team had weekly meetings and used a Google Doc to coordinated a calendar that included deadlines for the print stories, deadlines for the multimedia stories and deadlines for editing as well as other resources such as sources and contact information for a variety of city departments.

When all the content was in, the team previewed the project, making small adjustments until the very end.
