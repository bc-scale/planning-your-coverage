# 7. A Case Study

[**The Way Home: Journey through the Juvenile Justice System**](http://www.juvenilejusticejourneys.com/) -- Thousands of young people pass through the New York State juvenile justice system each year. It’s a patchwork of programs and facilities that costs the state and New York City millions of dollars annually. These are the stories of young people’s journeys through what is often a confusing system and of their efforts to get back on track.

This award-winning project was produced for the [NY City News Service](http://www.nycitynewsservice.com/) by Students at the [CUNY Graduate School of Journalism](http://www.journalism.cuny.edu/). Below is an outline of the planning and production process, along with some troubleshooting tips. This is a very elaborate project but should work to give smaller news organizations and even individual reporters valuable information about how to organize a solid coverage plan.

Juvenile justice was selected as a theme by editors because it is an important news story in New York City and reporters kept running into it while covering other stories. It is an issue with broad interest among News Service subscribers and a topic well within the organization’s editorial focus.

Once the topic had been selected, the coverage team developed a single paragraph summary of what the project was about. This summary helped keep the team focused as they worked through the coverage plan.

> “Thousands of young people pass through the New York State juvenile justice system each year. It’s a patchwork of programs and facilities that costs the state and New York City millions of dollars annually. These are the stories of young people’s journeys through what is often a confusing system and of their efforts to get back on track.”

With the broad theme selected, News Service staff started working on the coverage. This began with an ideas meeting, often called a brainstorming session. Three editors and ten reports first broke the coverage plan into three parts designed to move the viewer chronologically through the juvenile justice system in the same way as the characters, giving the project a strong story structure:

1. Picked Up (getting arrested),
2. Navigating the System (what happens from arrest to court to being locked up)
3. Moving On (getting released)

Each reporter was expected to research two ideas and present them at the brainstorming session. Editors than auditioned the ideas from reporters. In each story pitch the reporter was required to address the following coverage issues:

* print story summary
* multimedia (photo, audio, video) components
* crowdsourcing potential Assign stories

---
