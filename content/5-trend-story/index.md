# 5. The Trend Story

### The trend story is a story about an issue of ongoing importance to your viewers. It is a story with a life not necessarily tied to a single news event. Trends are challenging to cover because it can be hard to keep your coverage fresh over time.

---

## Find a hook

While trend stories go beyond a single event, it is wise to try and find a “hook” for your story. A hook is an event that pushes the issue into the news spotlight. This will give a focus to your coverage and provide a natural bump in audience interest. A hook could be the announcement of a change in law or policy related to the event. A hook could be something that happened to illustrate the issue. A hook could be an anniversary of an important date related to the issue.

---

## Provide regular updates

What if several weeks have gone by, and there is no hook for your story? It is important to keep up regular coverage of trends. You might provide a regular update or aggregate the best of the material produced by your audience and other sources on a regular schedule, say once a week. The important thing is that you need to be consistent so your audience knows when and what to expect.

---

## Curate conversation

Part of your job as a journalist in a social-first environment is to encourage discussion among your audience. Even if you do not have anything new to post, you should be part of the discussion about the issue all the time. Comment on the posts of others. Provide information to encourage more informed debate.

---

## Go deeper

Perhaps the big advantage in coverage of trend stories is that you have the luxury of time. You can do research, schedule interviews to produce more thoughtful content. Perhaps even do some investigative stories. If poor schools is an issue of importance to your audience, you could produce a series of five videos that showcases different elements of the issues like chapters in a book. You could post one video a week.

---
