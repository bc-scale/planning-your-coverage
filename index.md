# Planning Your Coverage

![](images/cover.jpg)

Most stories are based on regular coverage of scheduled events -- a speech, a performance or an election. Reporters and news organizations can plan their coverage hours, days, or weeks in advance. Other stories happen unexpectedly as breaking news and journalists need to respond immediately. Some stories resolve quickly. Others may require coverage for months. No matter the story it is important to have a strategy for coverage -- a clear plan for reporting the story from beginning to end.

In a social-first environment coverage will undoubtedly involve your audience of contributors. Your audience may be the initial source for your story and go on to contribute to your coverage. With breaking news, reporters and social editors need to quickly engage the audience and leverage its knowledge and interest to begin coverage. For planned events, reporters and news organizations need to engage the audience before and during the event, building interest as the event approaches and over the life of the event.

---
